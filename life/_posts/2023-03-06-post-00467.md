---
layout: post
title: "[ Mastering Bitcoin ] 04. 비트코인의 키와 주소"
toc: true
---

 원문 : https://github.com/bitcoinbook/bitcoinbook
 

 * 복습할 동안 정리했던 기입 업로드 합니다. 중요하지 않은 부분은 제외하고 개념과 길 위주로 정리했어요.
 

 

 ​

## 키
 Key
 Most bitcoin transactions require a valid digital signature to be included in the blockchain, which can only be generated with a secret key; therefore, anyone with a copy of that key has control of the bitcoin.
 모든 비트코인 거래에는 블록체인에 포함되기 위한 유효한 서명이 필요하며, 이러한 서명은 유효한 디지털 키가 있어야 형성될 요행 있다.
 

 The recipient’s public key is represented by its digital fingerprint, called a Bitcoin address.
 비트코인 수취인의 공개키는 비트코인 주소라고 불리는 디지털 지문으로 표현된다.
 

 ​
 ​
 

 개인키로 공개키를 만들고, 공개키로 비트코인 주소를 만든다.
 반면 비트코인 주소로 공개키를 알아낼 요체 없고, 공개키로 개인키를 알아낼 복운 없다.
 

 ​

## 개인키
 Private Keys
 A private key is simply a number, picked at random.
 개인키는 무작위로 추출된 숫자로 구성되어 있다.
 

 The private key must also be backed up and protected from accidental loss, because if it’s lost it cannot be recovered and the funds secured by it are forever lost, too.
 개인 키를 잃어버리면 자신의 비트코인에 대한 소유권을 잃게 되므로 철저한 관리가 필요하다.
 ​
 

## 공개키
 Public Keys
 The public key [비트코인](https://snap-haircut.com/life/post-00039.html) is calculated from the private key using elliptic curve multiplication.
 공개키는 타원 곡선 함수를 사용하여 개인키로부터 계산하며, 타인에게 알려지는 키로 전자 서명을 할 계기 필요한 정보이다.
 

 ​
 

 ​
 

 아무 생령 A를 두 고민 곱으로 나타낼 때 (A = B * C), 두 행복 B와 C를 인수라 하고 B와 C가 소수일 경위 소인수라고 한다.
 소인수가 족히 큰 제일 경우, 인수 분해하기가 넉넉히 어렵다는 원리를 이용한 암호 기법
 

 ​
 2. ECC (Elliptic Curve Cryptography) : 타원 곡선 공개키 암호
 ​
 

 ​
 

## 비압축형 공개키
 Uncompressed public keys
 K = k * G, where k is the private key, G is a constant point called the generator point , and K is the resulting public key. (k = 개인키, G = 상수, K = 공개키)
 이렇게 얻어진 공개키 K는 제한 쌍의 좌표값(x, y)이 된다.
 이때의 x값과 y값을 쉬이 붙인 형식이 비압축형 공개키이며, 비압축형 공개키(0x04)로 표현된다.
 

## 압축형 공개키
 Compressed public keys
 비트코인 타원 곡선 함수 정의식
 

 

 x값을 알면 정의식을 계산해 y값을 구할 행복 있다.
 따라서, y값은 생략하고 x값만 저장하면 절반의 공간을 줄일 생목숨 있다.
 하지만, y^2는 제곱근이기 그렇게 두 종류 값(짝수, 홀수)이 나오게 된다. 이때, 짝수는 (0x02), 홀수는 (0x03)로 표현된다.
 

 ​
 

 ​
 

 

## 비트코인 주소
 The Bitcoin address
 The Bitcoin address is derived from the public key through the use of one-way cryptographic hashing.
 비트코인 주소는 공개키에 의해 생성
 

 Conversion of a public key into a Bitcoin address
 공개키를 비트코인 주소로 변환하는 과정
 

 ​
 1) Hash Algorithm(SHA256, RIPEMD160)을 거쳐 160비트의 출력값을 생성
 2) Base58Check Encode 과정을 거쳐 비트코인 어드레스 생성
 ​
 

 Base58 and Base58Check Encoding
 Base64 - Binary Data를 Text로 변경
 Base58 - Base64 without the 0 (number zero), O (capital o), l (lower L), I (capital i), and the symbols “``” and "/”.
 Base64에서 0(숫자)/O(대문자), l(소문자)/I(대문자), ‘’, / 를 제외한 것
 

 ​
