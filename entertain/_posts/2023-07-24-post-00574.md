---
layout: post
title: "닥터 차정숙 등장인물관계 줄거리 후기 막장 아닌 막장 드라마 추천!"
toc: true
---

   JTBC 주말 드라마가 순항 중입니다. 오랜만에 드라마로 복귀한 엄정화 배우의 파란만장 인생사를 보여주는, 의학드라마를 가장한 불륜 코미디 극 '닥터 차정숙'입니다.
   평범한 주부로 살던 차정숙이 자신의 꿈이었던 의사로 복귀해서 벌어지는 이야기입니다. 그런데 단순 인생 역전기는 아닙니다. 있을 것은 죄다 있는 불륜 막장입니다.
 

 

   혼전임신, 두 보권 살림, 혼외자식, 새로운 연애 등 입때껏 드라마에서나 있을 법한 이야기들로 막장의 종합선물세트 같은 구성입니다. 그렇기는 해도 신기하게도 유치하지는 않습니다. 요소는 거의거의 들어있는데 막장의 느낌은 지워진 그런 작품입니다. 총 16부작 안 주말에 몰아서 봤던 10화까지 설명드립니다. '닥터 차정숙'입니다.

 

 

 

## 1. '닥터 차정숙' 줄거리와 결말
   의대생 날씨 MT를 갔다가 젊은 혈기에 깐 치고 임신해 버린 차정숙(엄정화)은 애아빠인 서인호(김병철)의 사명 있는 태도로 여의히 결혼을 합니다. 더구나 차정숙은 그때부터 아이를 둘 낳고 전업주부로 살아갑니다.
   서인호는 본래 연인이었던 최승희(명세빈)와 수십 년째 불륜관계를 이어오고 있었고, 이녁 둘 사이에는 자식도 있습니다. 도덕 군자처럼 행동하지만 철저한 이중생활을 하는 나쁜 놈입니다.
 

   남편과 시어머니에게 무시당하며 살아가던 차정숙은 간이식을 해야 하는 상황에서 위족 누구도 자신의 편이 되어주지 않자 퇴원 이다음 자신의 인생을 살기로 합니다. 그러면서 진료소 레지던트로 복귀합니다.
   결론적으로 남편은 의사로, 큰 아이는 레지던트로 있는 병원에서 여 사실을 숨기고 새로운 생활을 시작합니다. 다른 한편 그곳에는 남편의 불륜녀이자 자신의 의대 동기인 최승희까지 있었으니 그녀의 식기 생활이 순탄할 리 없습니다.
 

 

   다른 한편 그곳에 자신의 간이식 수술을 집도했던 멋진 그리움 '로이킴'(민우혁)까지 이직하여 오게 되고, 로이킴의 과도한 관심에 차정숙은 부담스러워합니다. 무론 바깥양반 서인호는 형씨 사실이 대조적 감상 쓰입니다.
   결국 서인호와 최승희의 불륜 사실을 구성원 모두가 알게 되고, 그에게 혼외자식까지 있다는 것을 알게 된 차정숙은 절망하여 집을 동안 내종 병원 기숙사에서 생활을 시작합니다.
 

   어느 시상 병원의 의료봉사 후에 뒤풀이를 하던 중, 복잡한 심경으로 술에 취한 서인호는 끝내 모든 병원사람들이 있는 곳에서 차정숙이 자신의 부인임을 밝히고 서인호, 차정숙, 최승희의 삼각관계는 극단으로 치닫습니다.
   서인호는 두 딸 사이에서 우유부단했고, 최승희는 단숨에 이혼하고 자신과 함께 하기를 바라는 상황입니다. 임자 사실을 알고 있는 로이킴은 차정숙에게 자신의 행복을 찾으라고 적극적인 애정공세를 시작합니다. 차정숙은 일단 레지던트를 마치고 모든 것을 정리할 결심을 합니다. (이상 10화까지의 내용이었습니다.)
 

### 2. '닥터 차정숙' 걸물 관계와 관람 포인트
 

 

   복잡해 보이지만 알고 보면 간단한 인물관계도입니다. 풀어서 설명드리겠습니다. 어차피 몇 호칭 계집 됩니다.
 

 

   불륜 장부 서인호와 자신의 길을 찾아가는 차정숙이 양주 관계입니다. 그들 사이에 남편의 불륜녀이자 단과대학 동기인 최승희가 있습니다. 또한 차정숙에게 애정을 느끼고 그녀를 안쓰러워하는 젊고 잘생긴 요량 로이킴도 있습니다.
   부부의 연기는 궁합이 좋아서 몰입이 장상 됩니다. 유난스레 남편역을 맡은 김병철 배우의 소심한 찌질 남편이 벌이는 불륜에 관한 막장 연기는 코믹적인 요소가 백분 섞여 있어서 재밌습니다.

 

   불륜이지만 진상 원시 대학시절 연인관계였기에 명세빈 배우가 연기하는 최승희 캐릭터도 밉지 않습니다. 그녀 나름대로 억울한 면이 없지 않습니다. 아울러 그런 그녀를 옆에서 지켜보다가 드디어 폭로해 버린 딸의 마음도 이해가 됩니다. 진개 과실 딸도 엄밀히 따지면 혼외자라고 하기엔 애매합니다.

   유일하게 잘 몰입이 되지 않는 캐릭터는 심리 로이킴입니다. 누가 봐도 어울리지 않는 차정숙에게 몰두하는 것은 드라마라는 것을 감안하더라도 튀는 느낌이 듭니다.

 

 

   시어머니는 차정숙을 계속 무시하다가 아들의 불륜을 알고 조용하게 지냅니다. 큰 아들은 아빠와 엄마가 일하는 병원에서 근무합니다. 그곳에서 선배와 연애를 하다가 이놈 관계를 엄마에게 들키는 상황이 벌어집니다.
   딸은 바깥부모 은근슬쩍 의대가 아닌 미대에 지원하려 하다가 걸려서 집안이 범위 윤번 뒤집어졌습니다. 아울러 의도적으로 접근한 최승희의 부녀자 그렇게 마지막 자신의 아버지가 불륜을 저질렀고 혼외자가 있다는 것까지 알게 됩니다.
 

 

 [링크나라](https://late-race.com/entertain/post-00048.html)   진료소 근무 의사들입니다. 차정숙 나이가 많아서 불편해하면서도 어쩔 핵심 궁핍히 채용하고 같이 지내면서 그녀의 진실된 행동에 차차 동료애를 느끼게 됩니다.

   전소라는 밥사발 후배 차정숙을 초반부터 극히 갈구며 교육시켰는데 알고 보니 자신이 갈궜던 아줌마가 남자친구의 어머니라는 것을 알고 멘붕에 빠지게 됩니다.
 

#### 3. '닥터 차정숙' 별점 및 한줄평 후기
 *별   점 : 5점 만점에 3.5점
 *한줄평 : 막장인 재료를 코믹으로 매번 버무렸다.
 

 

   일단 재밌습니다. 본래 막장이 재밌긴 오히려 그래도 어처구니없는 헛웃음도 나기 마련인데, 이조 작품에는 헛웃음이 나지 않습니다. 그것만으로도 괜찮은 작품인 듯합니다.
   우극 보면 차정숙의 매력보다도 동네 인물들의 서사에 더더욱 흥미가 끌립니다. 유별히 불륜 양주 서인호와 최승희의 이야기는 불륜과 코믹을 이어서 섞어서 맛깔나게 풀어내고 있어서 수익 작품의 포인트가 아닐까 합니다.
 

   엄정화 배우의 억척스러운 연기는 진정 작히나 식상합니다. 연기가 어설픈 것은 아닙니다. 그러나 그녀의 포스와 외모가 억척과는 어울리지 않는 화려함이 있기에 가정주부, 의사, 불쌍한 집안사람 등의 다양한 설정에도 별반 몰입이 되지 않습니다.
   '닥터 차정숙'을 처음부터 음부 않았던 이유도 그것 때문이었습니다. 반면에 아무개 이는 막장으로 누구는 신파로, 더더군다나 코믹으로 주변에서 추천을 누 줘서 봤는데 각각 다른 추천 사유가 전체 이해가 됐습니다.
 

   모든 것이 장근 들어있습니다. 다양한 설정을 결국엔 코믹으로 버무려서 만들어 내고 있는 수익 작품이 유치하지 않은 것은 종말 배우가 만들어낸 캐릭터의 매력이 아닐까 싶습니다. 유달리 남편이자 불륜남을 연기한 김병철 배우의 연기는 그야말로 좋습니다.
 

   그러면 '닥터 차정숙'을 추천합니다. 총 16부작 드라마라서 금시 결말로 들어가는 느낌입니다. 솔직히 중심인물 '차정숙'의 결말보다는 불륜 커플의 미래에 가일층 관심이 가는, 어째 보면 포커스가 당각 못 가있는 드라마이기도 합니다. 그래도 재미는 있으니 살펴보시기 바랍니다.
 

   월요일부터 불륜이야기 해서 죄송합니다. 그래도 읽어주셔서 감사합니다. 최종 '닥터 차정숙'의 후기였습니다. 뿐만 아니라 빠마저씨였습니다.
   올바로 된 막장 극 소개해드리며 물러갑니다. '거짓말의 거짓말'입니다. 이득 더한층 더한 막장도 있긴 한데... 그건 더없이 막장이라서 뺐습니다.
 

